// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Foad Nachabe (foadn)
package prj5;

import java.awt.Color;
import CS2114.Button;
import CS2114.Shape;
import CS2114.TextShape;
import CS2114.Window;
import CS2114.WindowSide;

/**
 * The GUI_DisplayWindow class is responsible for connecting the buttons and
 * data into the window that is displayed for the consumer to use. Gives
 * Functionality to all buttons
 * 
 * @author Foad Nachabe <foadn>
 * 
 * @version <Novemeber 17, 2019>
 *
 */
public class GUI_DisplayWindow {

    /**
     * The Organizer that makes the data usable
     */
    private Organizer organizer;

    /**
     * The window that displays all the info
     */
    private Window window;

    /**
     * The previous page button on the top of the window
     */
    private Button prevT;

    /**
     * The sort by artist button on the top of the window
     */
    private Button artistT;

    /**
     * The sort by title button on the top of the window
     */
    private Button titleT;

    /**
     * The sort by release year button on the top of the window
     */
    private Button yearT;

    /**
     * The sort by genre button on the top of the window
     */
    private Button genreT;

    /**
     * The next page button on the top of the window
     */
    private Button nextT;

    /**
     * The hobby representation button on the bottom of the window
     */
    private Button hobbyB;

    /**
     * The major representation button on the bottom of the window
     */
    private Button majorB;

    /**
     * The region representation button on the bottom of the window
     */
    private Button regionB;

    /**
     * The quit button on the bottom of the window
     */
    private Button quitB;

    /**
     * The most recently chosen student sorting method
     * chosen by the user.
     */
    private String studentFilterState;

    /**
     * The most recently chosen song sorting method
     * chosen by the user.
     */
    private String songFilterState;


    /**
     * This is the constructor for the display window. It will give
     * Functionality to all the buttons and give them names
     */
    public GUI_DisplayWindow(Organizer organizer) {
        // Initializing all the fields
        window = new Window("Project 5");
        window.setSize(1200, 700);
        prevT = new Button("<--- Prev");
        artistT = new Button("Sort by Artist");
        titleT = new Button("Sort by Song Title");
        yearT = new Button("Sort by Release Year");
        genreT = new Button("Sort by Genre");
        nextT = new Button("Next --->");
        hobbyB = new Button("Represent Hobby");
        majorB = new Button("Represent Major");
        regionB = new Button("Represent Region");
        quitB = new Button("Quit");
        // Adding all the buttons to the window
        window.addButton(prevT, WindowSide.NORTH);
        window.addButton(artistT, WindowSide.NORTH);
        window.addButton(titleT, WindowSide.NORTH);
        window.addButton(yearT, WindowSide.NORTH);
        window.addButton(genreT, WindowSide.NORTH);
        window.addButton(nextT, WindowSide.NORTH);
        window.addButton(hobbyB, WindowSide.SOUTH);
        window.addButton(majorB, WindowSide.SOUTH);
        window.addButton(regionB, WindowSide.SOUTH);
        window.addButton(quitB, WindowSide.SOUTH);
        // Giving all the buttons their functions
        hobbyB.onClick(this, "clickedRHobby");
        majorB.onClick(this, "clickedRMajor");
        regionB.onClick(this, "clickedRRegion");
        quitB.onClick(this, "clickedQuit");
        artistT.onClick(this, "clickedSortArtist");
        titleT.onClick(this, "clickedSortTitle");
        yearT.onClick(this, "clickedSortYear");
        genreT.onClick(this, "clickedSortGenre");
        nextT.onClick(this, "clickedNext");
        prevT.onClick(this, "clickedPrev");
        // Initialize default organizer behavior
        this.organizer = organizer;
        this.organizer.sortSongsBy("title");
        this.organizer.showByHobby();
        this.songFilterState = "title";
        this.studentFilterState = "h";
        this.prevT.disable();
        legend();
        glyph();
        // -------------------------------------
    }


    /**
     * This will create the legend on the display window and will change
     * determined by the different representations of the data
     */
    public void legend() {
        // Makes the box around the legend
        Shape rectangle1 = new Shape(700 + 400, 100 + 100, 3, 180, Color.BLACK);
        Shape rectangle2 = new Shape(595 + 400, 100 + 100, 3, 180, Color.BLACK);
        Shape rectangle3 = new Shape(595 + 400, 100 + 100, 108, 3, Color.BLACK);
        Shape rectangle4 = new Shape(595 + 400, 278 + 100, 108, 3, Color.BLACK);
        // Assigns the default legend
        TextShape legend = new TextShape(600 + 400, 105 + 100, "Hobby Legend",
            Color.BLACK);
        TextShape hobby1 = new TextShape(600 + 400, 125 + 100, "Read",
            Color.MAGENTA.brighter());
        TextShape hobby2 = new TextShape(600 + 400, 145 + 100, "Art", Color.BLUE
            .darker());
        TextShape hobby3 = new TextShape(600 + 400, 165 + 100, "Sport",
            Color.ORANGE);
        TextShape hobby4 = new TextShape(600 + 400, 185 + 100, "Music",
            Color.GREEN.brighter());
        TextShape songTitle = new TextShape(615 + 400, 215 + 100, "Song Title",
            Color.BLACK);
        Shape barrier = new Shape(650 + 400, 225 + 100, 3, 50, Color.BLACK);
        TextShape heard = new TextShape(600 + 400, 245 + 100, "Heard",
            Color.BLACK);
        TextShape like = new TextShape(665 + 400, 245 + 100, "Like",
            Color.BLACK);
        // Sets the colors for the different shapes
        rectangle1.setBackgroundColor(Color.WHITE);
        rectangle2.setBackgroundColor(Color.WHITE);
        rectangle3.setBackgroundColor(Color.WHITE);
        rectangle4.setBackgroundColor(Color.WHITE);
        legend.setBackgroundColor(Color.WHITE);
        hobby1.setBackgroundColor(Color.WHITE);
        hobby2.setBackgroundColor(Color.WHITE);
        hobby3.setBackgroundColor(Color.WHITE);
        hobby4.setBackgroundColor(Color.WHITE);
        songTitle.setBackgroundColor(Color.WHITE);
        heard.setBackgroundColor(Color.WHITE);
        like.setBackgroundColor(Color.WHITE);
        // The switch case responsible for changing the legend based on the
        // different representation
        switch (this.studentFilterState) {
            case "h":
                // The hobby legend case
                legend.setText("Hobby Legend");
                hobby1.setText("Read");
                hobby2.setText("Art");
                hobby3.setText("Sport");
                hobby4.setText("Music");
                break;
            case "m":
                // The major legend case
                legend.setText("Major Legend");
                hobby1.setText("Comp Sci");
                hobby2.setText("Other Eng");
                hobby3.setText("Math/CMDA");
                hobby4.setText("Other");
                break;
            case "r":
                // The region legend case
                legend.setText("Region Legend");
                hobby1.setText("NorthEast US");
                hobby2.setText("SouthEast US");
                hobby3.setText("Rest of US");
                hobby4.setText("Outside of US");
                break;
            default:
                // The default hobby legend
                legend.setText("Hobby Legend");
                hobby1.setText("Read");
                hobby2.setText("Art");
                hobby3.setText("Sport");
                hobby4.setText("Music");
                break;
        }
        // Adds the legend shapes to the window
        window.addShape(rectangle1);
        window.addShape(rectangle2);
        window.addShape(rectangle3);
        window.addShape(rectangle4);
        window.addShape(legend);
        window.addShape(hobby1);
        window.addShape(hobby2);
        window.addShape(hobby3);
        window.addShape(hobby4);
        window.addShape(songTitle);
        window.addShape(barrier);
        window.addShape(heard);
        window.addShape(like);
    }


    /**
     * This will create the data representation glyphs for the entire window
     */
    public void glyph() {
        Figure[] figs = organizer.getFigures();
        // Makes sure that none of the figures are null

        for (Figure fig : figs) {
            if (fig == null) {
                break;
            }
            PositionEnum pos = fig.getPosition();

            // This will give the glyphs their locations
            switch (pos) {
                case TLEFT: {
                    setGlyph(200, 150, fig);
                    break;
                }
                case TMIDDLE: {
                    setGlyph(500, 150, fig);
                    break;
                }
                case TRIGHT: {
                    setGlyph(800, 150, fig);
                    break;
                }
                case MLEFT: {
                    setGlyph(200, 300, fig);
                    break;
                }
                case MMIDDLE: {
                    setGlyph(500, 300, fig);
                    break;
                }
                case MRIGHT: {
                    setGlyph(800, 300, fig);
                    break;
                }
                case BLEFT: {
                    setGlyph(200, 450, fig);
                    break;
                }
                case BMIDDLE: {
                    setGlyph(500, 450, fig);
                    break;
                }
                case BRIGHT: {
                    setGlyph(800, 450, fig);
                    break;
                }
                default: {
                    setGlyph(300, 300, fig);
                    break;
                }
            }
        }
    }


    /**
     * This is a helper method designed to be used in tandem with
     * the PositionEnum class to create shapes and add them at the
     * relevant positions in the Window.
     * 
     * X and Y indicate the center of the graph region, and shape
     * positions are determined relative to that center position by a
     * fixed amount consistent for all graph regions.
     * 
     * There is also a scaling constant used for stylistic reasons
     * which multiplies the length of all bar graphs
     * by said constant.
     * 
     * @param x
     *            The center x position for the graph region
     * @param y
     *            the center y position for the graph region
     * @param fig
     *            The figure with the relevant statistics for
     *            this specific graph region.
     * 
     * @author Pau Lleonart Calvo
     */
    private void setGlyph(int x, int y, Figure fig) {
        double scaleConstant = 1.4;

        // Note: Heard on left, like on right

        // Create Header
        TextShape header = new TextShape(x, y - 30, fig.getTitle(),
            Color.BLACK);
        TextShape header2;

        switch (this.songFilterState) {
            case "title":
                header2 = new TextShape(x, y - 30, "by " + fig.getArtist(),
                    Color.BLACK);
                break;
            case "artist":
                header2 = new TextShape(x, y - 30, "by " + fig.getArtist(),
                    Color.BLACK);
                break;
            case "year":
                header2 = new TextShape(x, y - 30, "Year: " + fig.getYear(),
                    Color.BLACK);
                break;
            case "genre":
                header2 = new TextShape(x, y - 30, "Genre: " + fig.getGenre(),
                    Color.BLACK);
                break;
            default:
                header2 = new TextShape(x, y - 30, "by " + fig.getArtist(),
                    Color.BLACK);
                break;
        }

        header.setX(x - (header.getWidth() / 2));
        header.setY(y - header.getHeight() - 50);
        header.setBackgroundColor(Color.WHITE);

        header2.setX(x - (header2.getWidth() / 2));
        header2.setY(y - header2.getHeight() - 35);
        header2.setBackgroundColor(Color.WHITE);

        // Create separator
        Shape separator = new Shape(x - 2, y - 30, 4, 60, Color.BLACK);

        // Create top set of bars
        Shape bar1a = new Shape(x - (int)(fig.getHeard(1) * scaleConstant), y
            - 30, (int)(fig.getHeard(1) * scaleConstant), 15, Color.MAGENTA);
        Shape bar1b = new Shape(x, y - 30, (int)(fig.getLiked(1)
            * scaleConstant), 15, Color.MAGENTA);

        // Create second set of bars
        Shape bar2a = new Shape(x - (int)(fig.getHeard(2) * scaleConstant), y
            - 15, (int)(fig.getHeard(2) * scaleConstant), 15, Color.BLUE);
        Shape bar2b = new Shape(x, y - 15, (int)(fig.getLiked(2)
            * scaleConstant), 15, Color.BLUE);

        // Create third set of bars
        Shape bar3a = new Shape(x - (int)(fig.getHeard(3) * scaleConstant), y,
            (int)(fig.getHeard(3) * scaleConstant), 15, Color.ORANGE);
        Shape bar3b = new Shape(x, y, (int)(fig.getLiked(3) * scaleConstant),
            15, Color.ORANGE);

        // Create fourth set of bars
        Shape bar4a = new Shape(x - (int)(fig.getHeard(4) * scaleConstant), y
            + 15, (int)(fig.getHeard(4) * scaleConstant), 15, Color.GREEN);
        Shape bar4b = new Shape(x, y + 15, (int)(fig.getLiked(4)
            * scaleConstant), 15, Color.GREEN);

        // Add all shapes to window
        this.window.addShape(header);
        this.window.addShape(header2);
        this.window.addShape(separator);
        this.window.addShape(bar1a);
        this.window.addShape(bar1b);
        this.window.addShape(bar2a);
        this.window.addShape(bar2b);
        this.window.addShape(bar3a);
        this.window.addShape(bar3b);
        this.window.addShape(bar4a);
        this.window.addShape(bar4b);

    }


    /**
     * This is the button that will change the info to represent the hobby info
     * 
     * @param button
     *            the button being clicked
     */
    public void clickedRHobby(Button button) {
        this.studentFilterState = "h";
        this.updateWindow();
    }


    /**
     * This is the button that will change the info to represent the major info
     * 
     * @param button
     *            the button being clicked
     */
    public void clickedRMajor(Button button) {
        this.studentFilterState = "m";
        this.updateWindow();
    }


    /**
     * This is the button that will change the info to represent the major info
     * 
     * @param button
     *            the button being clicked
     */
    public void clickedRRegion(Button button) {
        this.studentFilterState = "r";
        this.updateWindow();
    }


    /**
     * This will exit the the entire window
     * 
     * @param button
     *            the button being clicked
     */
    public void clickedQuit(Button button) {
        System.exit(0);
    }


    /**
     * This will change the displayed list to sort by the artist's name
     * 
     * @param button
     *            the button being clicked
     * 
     * @author Pau Lleonart Calvo
     */
    public void clickedSortArtist(Button button) {
        this.songFilterState = "artist";
        this.updateWindow();
    }


    /**
     * This will change the displayed list to sort by the song title
     * 
     * @param button
     *            the button being clicked
     * 
     * @author Pau Lleonart Calvo
     */
    public void clickedSortTitle(Button button) {
        this.songFilterState = "title";
        this.updateWindow();
    }


    /**
     * This will change the displayed list to sort by the song year
     * 
     * @param button
     *            the button being clicked
     * 
     * @author Pau Lleonart Calvo
     */
    public void clickedSortYear(Button button) {
        this.songFilterState = "year";
        this.updateWindow();
    }


    /**
     * This will change the displayed list to sort by the song genre
     * 
     * @param button
     *            the button being clicked
     * 
     * @Pau Lleonart Calvo
     */
    public void clickedSortGenre(Button button) {
        this.songFilterState = "genre";
        this.updateWindow();
    }


    /**
     * The clickedNext method updates the window
     * with the next set of 9 songs (or however many are left) if there is a
     * next page.
     * If there are not pages left, the button disables itself.
     * 
     * It is re-enabled once the prevT button is clicked.
     * 
     * @param button
     *            The button that has been clicked (the nextT button)
     * 
     * @author Pau Lleonart Calvo
     */
    public void clickedNext(Button button) {
        this.organizer.nextPage();
        this.prevT.enable();
        this.updateWindow();
        if (!organizer.hasMorePages()) {
            this.nextT.disable();
        }
    }


    /**
     * The clickedPrev method updates the window
     * with the previous set of 9 songs if there is a previous page.
     * If the user cannot go back more pages, the button disables itself.
     * 
     * It is re-enabled once the next button is clicked.
     * 
     * @param button
     *            The button that has been clicked (the prevT button)
     * 
     * @author Pau Lleonart Calvo
     */
    public void clickedPrev(Button button) {
        if (organizer.getPage() > 1) {
            organizer.previousPage();
        }
        else {
            organizer.previousPage();
            this.prevT.disable();
        }
        this.nextT.enable();
        this.updateWindow();
    }


    /**
     * The updateWindow method updates the window (wow) with
     * the latest data if the user ever chooses to change pages or
     * the sorting methods for students or songs.
     * 
     * All shapes are removed and then replaced with their updated
     * counterparts.
     * 
     * @author Pau Lleonart Calvo
     */
    public void updateWindow() {
        this.window.removeAllShapes();

        switch (this.songFilterState) {
            case "title":
                this.organizer.sortSongsBy("title");
                break;
            case "artist":
                this.organizer.sortSongsBy("artist");
                break;
            case "year":
                this.organizer.sortSongsBy("year");
                break;
            case "genre":
                this.organizer.sortSongsBy("genre");
                break;
            default:
                this.organizer.sortSongsBy("title");
                break;
        }

        switch (this.studentFilterState) {
            case "h":
                this.organizer.showByHobby();
                break;
            case "m":
                this.organizer.showByMajor();
                break;
            case "r":
                this.organizer.showByRegion();
                break;
            default:
                this.organizer.showByHobby();
                break;
        }
        this.legend();
        this.glyph();
    }
}