package prj5;

import student.TestCase;

/**
 * Tests the Figure class' methods.
 * 
 * @author Pau Lleonart Calvo (paullc)
 * @version 12/1/2019
 *
 */
public class FigureTest extends TestCase {
    
    /**
     * Figure object field for testing
     */
    private Figure figure;
    
    
    /**
     * Empty Figure constructor
     */
    public FigureTest() {
        //Empty constructor
    }
    
    /**
     * SetUp test method
     */
    public void setUp() {
        figure = new Figure(PositionEnum.TLEFT, 
            new double[] {
                10, 20, 30, 40, 50, 60, 70, 80
                }, 
            "title", "artist", "year", "genre");
    }
    
    /**
     * Tests the Figure getArtist() method.
     */
    public void testGetArtist() {
        
        assertEquals("artist", figure.getArtist());
    }
    
    /**
     * Tests the Figure getGenre() method.
     */
    public void testGetGenre() {
        assertEquals("genre", figure.getGenre());
    }
    
    /**
     * Tests the Figure getHeard() method.
     */
    public void testGetHeard() {
        assertEquals(10, figure.getHeard(1));
        assertEquals(20, figure.getHeard(2));
        assertEquals(30, figure.getHeard(3));
        assertEquals(40, figure.getHeard(4));
        assertEquals(10, figure.getHeard(5));
    }
    
    /**
     * Tests the Figure getLiked() method.
     */
    public void testGetLiked() {
        assertEquals(50, figure.getLiked(1));
        assertEquals(60, figure.getLiked(2));
        assertEquals(70, figure.getLiked(3));
        assertEquals(80, figure.getLiked(4));
        assertEquals(50, figure.getLiked(5));
    }
    
    /**
     * Tests the Figure getPosition() method.
     */
    public void testGetPosition() {
        assertEquals(PositionEnum.TLEFT, figure.getPosition());
    }
    
    /**
     * Tests the Figure getTitle() method.
     */
    public void testGetTitle() {
        assertEquals("title", figure.getTitle());
    }
    
    /**
     * Tests the Figure getYear() method.
     */
    public void testGetYear() {
        assertEquals("year", figure.getYear());
    }
}
