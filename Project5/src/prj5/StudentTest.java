// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those
// who do.
// -- Nathan Craig (nlcraig)
package prj5;

import java.util.ArrayList;
import student.TestCase;

/**
 * @author Nathan Craig (nlcraig)
 * @version 2019.11.17
 *
 *          Test class that contains test cases for each method and possible
 *          method
 *          outcome for the methods contained in the Student class.
 *
 */
public class StudentTest extends TestCase {
    private Student testStudent;
    private ArrayList<Integer> likes;
    private ArrayList<Integer> heard;


    /**
     * Set up the conditions to be repeated for each test Case.
     */
    public void setUp() {
        likes = new ArrayList<Integer>();
        heard = new ArrayList<Integer>();
        testStudent = new Student(1, "CS", "Southeast", "Sports", heard, likes);
        likes.add(1);
        likes.add(0);
        likes.add(1);
        heard.add(0);
        heard.add(1);
        heard.add(0);
    }

    /**
     * Tests the get likes method for out of bounds index 
     */
    public void testGetLikesOutOfBounds() {
        assertEquals(-1, testStudent.getLikes(likes.size() + 5));
    }

    /**
     * Test the getter that is responsible for returning the personalNumebr
     * field of a Student object.
     */
    public void testGetPersonalNumber() {
        assertEquals(1, testStudent.getPersonalNumber());
    }


    /**
     * Test the getter that is responsible for returning the hobby field of a
     * Student object.
     */
    public void testGetHobby() {
        assertEquals("Sports", testStudent.getHobby());
    }


    /**
     * Test the getter that is responsible for returning the region field of
     * a Student object.
     */
    public void testGetRegion() {
        assertEquals("Southeast", testStudent.getRegion());
    }


    /**
     * Test the getter that is responsible for returning the major field of a
     * Student object.
     */
    public void testGetMajor() {
        assertEquals("CS", testStudent.getMajor());
    }


    /**
     * Test the toString method to ensure that Student objects render the
     * correctly formatted String representation of their attributes.
     */
    public void testToString() {
        assertEquals("[Number: 1 Hobby: Sports Region: Southeast Major: CS]",
            testStudent.toString());
    }


    /**
     * Test the overridden equals method of the Student class in the case that
     * the Object being compared to is null
     */
    public void testEqualsNull() {
        Object obj = null;
        assertFalse(testStudent.equals(obj));
    }


    /**
     * Test the overridden equals method of the Student class in the case that
     * the Object being compared to references the same location in memory
     * as the Student calling equals.
     */
    public void testEqualsIndentical() {
        Student compare = testStudent;
        assertEquals(testStudent, compare);
    }


    /**
     * Test the overridden equals method of the Student class in the cases that
     * a Student object being compared to has the same attributes (i.e is
     * equal) as the Student calling the method, and the cases that the two
     * Students have differing attributes;
     */
    public void testEquals() {
        Student compare1 = new Student(1, "CS", "Southeast", "Sports", heard,
            likes);
        assertEquals(testStudent, compare1);

        Student compare2 = new Student(1, "CS", "Southeast", "Reading", heard,
            likes);
        assertFalse(testStudent.equals(compare2));

        Student compare3 = new Student(1, "CS", "Northeast", "Sports", heard,
            likes);
        assertFalse(testStudent.equals(compare3));

        Student compare4 = new Student(1, "CMDA", "Southeast", "Sports", heard,
            likes);
        assertFalse(testStudent.equals(compare4));
        Student compare5 = new Student(2, "CMDA", "Southeast", "Sports", heard,
            likes);
        assertFalse(testStudent.equals(compare5));
    }


    /**
     * Test the overridden equals method of the Student class in the case that
     * the Object being compared to is not an instance of Student.
     */
    public void testEqualsDiffClass() {
        Object obj = new Object();
        assertFalse(testStudent.equals(obj));
    }


    /**
     * Test the compare methods of the Student class in the cases that
     * the attributes of the Student being compared to are the same and
     * different.
     */
    public void testCompare() {
        Student compare1 = new Student(2, "CS", "Southeast", "Sports", likes,
            likes);
        Student compare2 = new Student(3, "CMDA", "Northeast", "Reading", likes,
            heard);

        assertEquals(1, testStudent.compareHobby(compare1));
        assertEquals(-1, testStudent.compareHobby(compare2));

        assertEquals(1, testStudent.compareMajor(compare1));
        assertEquals(-1, testStudent.compareMajor(compare2));

        assertEquals(1, testStudent.compareRegion(compare1));
        assertEquals(-1, testStudent.compareRegion(compare2));
    }


    /**
     * Test the getLikes method to ensure it returns the correct value for
     * each index.
     */
    public void testGetLikes() {
        assertEquals(1, testStudent.getLikes(0));
        assertEquals(0, testStudent.getLikes(1));
        assertEquals(1, testStudent.getLikes(2));
    }


    /**
     * Test the getHeard method to ensure it returns the correct value for
     * each index.
     */
    public void testGetHeard() {
        assertEquals(0, testStudent.getHeard(0));
        assertEquals(1, testStudent.getHeard(1));
        assertEquals(0, testStudent.getHeard(2));
    }
}