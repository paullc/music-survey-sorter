// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Foad Nachabe (foadn)
package prj5;

import java.util.Iterator;
import java.util.NoSuchElementException;
import student.TestCase;

/**
 * This will test all the methods in the linkedlist class
 * 
 * @author Foad Nachabe <foadn>
 * @version <November 16, 2019>
 *
 */
public class LinkedListTest extends TestCase {
    private LinkedList<String> list;
    private Iterator<String> itr;
    private Exception exception;
    private String nullData;


    /**
     * This will create the setup for the test class
     */
    public void setUp() {
        list = new LinkedList<String>();
        itr = list.iterator();
        exception = new Exception();
        nullData = null;
    }


    /**
     * This will test the add method with one parameter
     */
    public void testAdd() {
        list.add("Movie 1");
        list.add("Movie 2");
        assertTrue(list.add("Movie 3"));
        assertEquals(list.size(), 3);
    }


    /**
     * This will test the add method with two parameters
     */
    public void testAdd2() {
        try {
            list.add(-1, "Not Movie");
        }
        catch (IndexOutOfBoundsException e) {
            exception = e;
        }
        assertNotNull(exception);
        try {
            list.add(1, "Not Movie");
        }
        catch (IndexOutOfBoundsException e) {
            exception = e;
        }
        assertNotNull(exception);
        try {
            list.add(0, nullData);
        }
        catch (IllegalArgumentException e) {
            exception = e;
        }
        assertNotNull(exception);
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 4");
        assertTrue(list.add(2, "Movie 3"));
        assertEquals(list.size(), 4);
        assertEquals(list.lastIndexOf("Movie 3"), 2);
    }


    /**
     * This will test the isEmpty method
     */
    public void testIsEmpty() {
        assertTrue(list.isEmpty());
        list.add("Movie 1");
        assertFalse(list.isEmpty());
    }


    /**
     * This will test the size method
     */
    public void testSize() {
        assertEquals(list.size(), 0);
        list.add("Movie 1");
        assertEquals(list.size(), 1);
        list.add("Movie 2");
        assertEquals(list.size(), 2);
        list.remove(1);
        assertEquals(list.size(), 1);
    }


    /**
     * This will test the clear method
     */
    public void testClear() {
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        assertEquals(list.size(), 3);
        list.clear();
        assertEquals(list.size(), 0);
    }


    /**
     * This will test the contains method
     */
    public void testContains() {
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        assertTrue(list.contains("Movie 3"));
        assertFalse(list.contains("Movie 4"));
    }


    /**
     * This will test the get method
     */
    public void testGet() {
        try {
            list.get(-1);
        }
        catch (IndexOutOfBoundsException e) {
            exception = e;
        }
        assertNotNull(exception);
        try {
            list.get(1);
        }
        catch (IndexOutOfBoundsException e) {
            exception = e;
        }
        assertNotNull(exception);
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        assertEquals(list.get(2), "Movie 3");
    }


    /**
     * This will test the lastIndexOf method
     */
    public void testLastIndexOf() {
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        list.add("Movie 3");
        assertEquals(list.lastIndexOf("Movie 2"), 1);
        assertEquals(list.lastIndexOf("Movie 3"), 3);
    }


    /**
     * This will test the remove method with the index parameter
     */
    public void testRemoveI() {
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        assertEquals(list.size(), 3);
        assertTrue(list.remove(1));
        assertFalse(list.contains("Movie 2"));
        assertEquals(list.size(), 2);
    }


    /**
     * This will test the remove method with the data parameter
     */
    public void testRemoveD() {
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        assertEquals(list.size(), 3);
        assertTrue(list.remove("Movie 2"));
        assertFalse(list.contains("Movie 2"));
        assertEquals(list.size(), 2);
        assertFalse(list.remove("Movie 7"));
    }


    /**
     * This will test the toString method
     */
    public void testToString() {
        assertEquals(list.toString(), "{}");
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        assertEquals(list.toString(), "{Movie 1, Movie 2, Movie 3}");
    }


    /**
     * This will test the hasNext method for the iterator class
     */
    public void testHasNext() {
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        assertTrue(itr.hasNext());
    }


    /**
     * This will test the next method for the iterator class
     */
    public void testNext() {
        try {
            itr.next();
        }
        catch (NoSuchElementException e) {
            exception = e;
        }
        assertNotNull(exception);
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        assertEquals(itr.next(), "Movie 1");
    }


    /**
     * This will test the remove method for the iterator class
     */
    public void testRemove() {
        try {
            itr.remove();
        }
        catch (IllegalStateException e) {
            exception = e;
        }
        assertNotNull(exception);
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        itr.next();
        itr.remove();
        assertFalse(list.contains("Movie 1"));
    }


    /**
     * This will test the remove method for the iterator class with, size = 1
     */
    public void testRemove2() {
        list.add("Movie 1");
        assertEquals(list.size(), 1);
        itr.next();
        itr.remove();
        assertEquals(list.size(), 0);
    }
    
    
    /**
     * This will test the swap method
     */
    public void testSwap() {
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        list.add("Movie 4");
        assertEquals(list.lastIndexOf("Movie 2"), 1);
        assertEquals(list.lastIndexOf("Movie 4"), 3);
        list.swap(1, 3);
        assertEquals(list.get(1), "Movie 4");
        assertEquals(list.get(3), "Movie 2");
    }
    
    
    /**
     * This will test the replace method 
     */
    public void testReplace() {
        list.add("Movie 1");
        list.add("Movie 2");
        list.add("Movie 3");
        list.add("Movie 4");
        assertEquals(list.lastIndexOf("Movie 3"), 2);
        list.replace(2, "Better Movie 3");
        assertEquals(list.get(2), "Better Movie 3");
        assertFalse(list.contains("Movie 3"));
    }
}