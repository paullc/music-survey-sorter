// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those 
// who do.
// -- Pau Lleonart Calvo (paullc)

package prj5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;

/**
 * The input class features two methods (readStudent and readSongs) which
 * read the student and song data files respectively and use the Student
 * and Song class to produce Linked Lists for each data set. 
 * 
 * The main method then initializes the GUI and inputs an Organizer parameter
 * which uses both the Student and Song data to organize and display data.
 * 
 * @author Pau Lleonart Calvo (paullc)
 * @version 11/17/2019
 *
 */
public class Input {
    
    /**
     * A Scanner object field used for reading the student
     * and song data files.
     */
    private static Scanner scanner;
    
    /**
     * The empty Input class constructor. 
     */
    public Input() {
        //Empty
    }


    /**
     * The readStudent method uses two scanners to scan each line of the
     * student data file and produce a Linked List of student objects to pass
     * into the Organizer in the main method.
     * 
     * @param fileName The name of the student data file to be scanned.
     * 
     * @return A LinkedList of Student objects to be used by the Organizer class.
     * 
     * @throws FileNotFoundException
     */
    public static LinkedList<Student> readStudent(String fileName) throws FileNotFoundException {
        scanner = new Scanner(new File(fileName));
        scanner.useDelimiter(",");
        LinkedList<Student> students = new LinkedList<Student>();
        scanner.nextLine();
        while (scanner.hasNextLine()) {
            
            
            int numId = Integer.parseInt(scanner.next().split(",")[0]);
            scanner.next();
            
            String major = scanner.next().toLowerCase().trim();
            String region = scanner.next().toLowerCase().trim();
            String hobby = scanner.next().toLowerCase().trim();
            
            Scanner lineScanner = new Scanner(scanner.nextLine());
            lineScanner.useDelimiter(",");
            
            ArrayList<Integer> heards = new ArrayList<Integer>();
            ArrayList<Integer> likes = new ArrayList<Integer>();
            
            while (lineScanner.hasNext()) {
                String heard = lineScanner.next();
                
                if (heard.equals("Yes")) {
                    heards.add(1);
                }
                else if (heard.equals("No")) {
                    heards.add(0);
                }
                else {
                    heards.add(-1);
                }
                
                if (lineScanner.hasNext()) {
                    
                    String like = lineScanner.next();
                    if (like.equals("Yes")) {
                        likes.add(1);
                    }
                    else if (like.equals("No")) {
                        likes.add(0);
                    }
                    else {
                        likes.add(-1);
                    }
                }
                
            }
            lineScanner.close();
            students.add(new Student(numId, major, region, hobby, heards, likes));
        }
        
        scanner.close();
        return students;
    }

    /**
     * The readSong method uses a scanner to scan each line of the
     * data file and produce a Linked List of song objects to pass
     * into the Organizer in the main method.
     * 
     * @param fileName The name of the song data file to be scanned.
     * 
     * @return A LinkedList of Song objects to be used by the Organizer class.
     * 
     * @throws FileNotFoundException
     */
    public static LinkedList<Song>  readSong(String fileName) throws FileNotFoundException {
        scanner = new Scanner(new File(fileName));
        
        LinkedList<Song> songs = new LinkedList<Song>();
        scanner.nextLine();
        
        int originalIndex = 0;
        while (scanner.hasNextLine()) {
            String[] songInfo = scanner.nextLine().split(",");
            
           
            String title = songInfo[0].trim();
            String artist = songInfo[1].trim();
            String year = songInfo[2].trim();
            String genre = songInfo[3].toLowerCase().trim();
            
            
            songs.add(new Song(title, artist, year, genre, originalIndex));
            originalIndex++;
        }
        scanner.close();
        return songs;
    }

    /**
     * The main method is the launch point of the Java application. 
     * This method initializes the music file and song file either by
     * default or by reading the arguments of the args String array.
     * 
     * @param args A String array which can contain arguments to be
     * used as musicFile and SongFile names.
     * 
     * @throws FileNotFoundException 
     */
    public static void main(String[] args) throws FileNotFoundException {
        String musicFile = "MusicSurveyData2019F.csv";
        String songFile = "SongList2019F.csv";
        
        if (args.length == 2) {
            musicFile = args[0];
            songFile = args[1];

        }
        Organizer organizer = new Organizer(readStudent(musicFile),
            readSong(songFile));
        GUI_DisplayWindow window = new GUI_DisplayWindow(organizer);

    }


}