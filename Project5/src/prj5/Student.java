// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those 
// who do.
// -- Nathan Craig (nlcraig)
package prj5;
import java.util.ArrayList;
/**
 * @author Nathan Craig (nlcraig)
 * @version 2019.11.16
 *
 * 
 * 
 */
public class Student
{
    private int personalNumber;
    private ArrayList<Integer> likes;
    private ArrayList<Integer> heard;
    private String hobby;
    private String region;
    private String major;
    
    /**
     * Constructor takes parameters for Student attributes and initializes
     * field variables such that they match the param inputs.
     * 
     * @param numId   The personal number for a Student.
     * @param hobby   Student's selected hobby preference.
     * @param region   Where a Student is from.
     * @param major   A Student's major of study.
     * @param heard The ArrayList of heard response values for this student
     * @param likes The ArrayList of likes response values for this student.
     */
    public Student(int numId, String major, String region, String hobby,
        ArrayList<Integer> heard, ArrayList<Integer> likes)
    {
        this.personalNumber = numId;
        this.hobby = hobby;
        this.region = region;
        this.major = major;
        this.likes = likes;
        this.heard = heard;
    }
    
    /**
     * Return the personal number that denotes the ordering of a Student and
     * the Student's responses to the music survey.
     * 
     * @return   The personal number of the Student.
     */
    public int getPersonalNumber()
    {
        return personalNumber;
    }
    
    /**
     * Return the hobby description for a Student.
     * 
     * @return   A Student's preferred hobby.
     */
    public String getHobby()
    {
        return hobby;
    }
    
    /**
     * Return the region description for a Student.
     * 
     * @return   A Student's region of origin.
     */
    public String getRegion()
    
    {
        return region;
    }
    
    /**
     * Return the major description for a student.
     * 
     * @return   A Student's collegiate major of study.
     */
    public String getMajor()
    {
        return major;
    }
    
    /**
     * Get the element stored in the likes array at a given index in order
     * to determine which way a Student responded to a survey question about
     * a specific song.
     * 
     * @return   The element at the given index.
     * @param index   The index to retrieve from.
     */
    public int getLikes(int index)
    {
        if (index >= likes.size()) {
            return -1;
        }
        return likes.get(index);
    }
    
    /**
     * Get the element stored in the heard array at a given index
     * in order to determine which way a Student responded to a survey
     * question about a specific Song.
     * 
     * @return   The element at the given index.
     * @param index   The index to retrieve from
     */
    public int getHeard(int index)
    {
        return heard.get(index);
    }
    
    /**
     * Return a string representation of a Student object in format
     * [Number: _ Hobby: _ Region: _ Major: _] where the underscores represent
     * the subsequent data that will be used to represent a Student.
     * 
     * @return   A string representation of a Student object.
     */
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("[Number: " + personalNumber + " Hobby: " + hobby 
            + " Region: " + region + " Major: " + major + "]");
        return builder.toString();
    }
    
    /**
     * Return a boolean based on the comparison of this Student and a parameter
     * Object. If the Object is an instance of the Student class, compare the
     * Student's on the basis of their personalNumber as well as the three
     * descriptive attributes for hobby, region, and major.
     * 
     * @return   True if the two Students are equal, false otherwise.
     */
    @Override
    public boolean equals(Object other)
    {
        if (other == null)
        {
            return false;
        }
        else if (other == this)
        {
            return true;
        }
        else if (this.getClass() == other.getClass())
        {
            Student cast = (Student) other;
            return cast.getPersonalNumber() == personalNumber && 
                hobby.equals(cast.getHobby()) && major.equals(cast.getMajor())
                && region.equals(cast.getRegion());
        }
        return false;
    }
    
    /**
     * Method for comparing the major attributes of Student objects for
     * equality.
     * 
     * @return   1 if the Students share the same major, -1 if they do not.
     * @param student   The student being compared to.
     */
    public int compareMajor(Student student)
    {
        if (major.equals(student.getMajor()))
        {
            return 1;
        }
        return -1;
    }
    
    /**
     * Method for comparing the region attributes of Student objects for
     * equality.
     * 
     * @return   1 if the Students share the same region of origin, -1 if 
     *           they do not.
     * @param student   The student being compared to.
     */
    public int compareRegion(Student student)
    {
        if (region.equals(student.getRegion()))
        {
            return 1;
        }
        return -1;
    }
    
    /**
     * Method for comparing the hobby attributes of Student objects for
     * equality.
     * 
     * @return   1 if the Students share the same preferred hobby, -1 if 
     *           they do not.
     * @param student   The student being compared to.
     */
    public int compareHobby(Student student)
    {
        if (hobby.equals(student.getHobby()))
        {
            return 1;
        }
        return -1;
    }
}
