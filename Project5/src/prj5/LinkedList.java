// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Foad Nachabe (foadn)
package prj5;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class will create a linkedList implemention using a private node and
 * private iterator class
 * 
 * @author Foad Nachabe (foadn)
 * @version <11/6/2019>
 *
 * @param <T>
 *            the generic variable
 */
public class LinkedList<T> implements Iterable<T> {
    /**
     * This is the private node class
     * 
     * @author Foad Nachabe (foadn)
     * @version <November 16, 2019>
     *
     * @param <T>
     *            the generic variable
     */
    private static class Node<T> {
        private T data;
        private Node<T> next;
        private Node<T> prev;


        /**
         * This is the constructor for the node class
         * 
         * @param data
         *            the data being passed in
         */
        public Node(T data) {
            this.data = data;
        }


        /**
         * This method will set next to the next node
         * 
         * @param next
         *            the next node
         */
        public void setNext(Node<T> next) {
            this.next = next;
        }


        /**
         * This method will set the prev to the previous node
         * 
         * @param previous the previous node
         */
        public void setPrevious(Node<T> previous) {
            this.prev = previous;
        }


        /**
         * This is the method for the getting the next node
         * 
         * @return the next node
         */
        public Node<T> next() {
            return next;
        }


        /**
         * This is the method for the getting the prev node
         * 
         * @return the prev node
         */
        public Node<T> previous() {
            return prev;
        }


        /**
         * This is the method for getting the data of the node
         * 
         * @return the data of the node
         */
        public T data() {
            return data;
        }
    }


    /**
     * The creates an inner private iterator class inside the linkedlist
     * 
     * @author Foad Nachabe <foadn>
     * @version <November 16, 2019>
     *
     * @param <E>
     *            the generic variable
     */
    private class LinkedListIterator<E> implements Iterator<T> {
        //private int nextPosition;
        private boolean wasNextCalled;
        private Node<T> currentNode;


        /**
         * The constructor of the iterator class
         */
        public LinkedListIterator() {
            //nextPosition = 0;
            wasNextCalled = false;
            currentNode = head;
        }


        /**
         * Returns true if the iterator has a next item
         */
        @Override
        public boolean hasNext() {
            return (currentNode.next().data() != null);
        }


        /**
         * Returns the next item
         */
        @Override
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            else {
                wasNextCalled = true;
                currentNode = currentNode.next();
                T data = currentNode.data();
                return data;
            }
        }


        /**
         * Removes the item that next gets
         */
        @Override
        public void remove() {
            if (!wasNextCalled) {
                throw new IllegalStateException("Next was never called");
            }
            if (size > 1) {
                currentNode.previous().setNext(currentNode.next);
                currentNode.next().setPrevious(currentNode.previous());
            }
            size--;
            wasNextCalled = false;
        }
    }

    // This is the code for the LinkedList
    private int size;
    private Node<T> head;
    private Node<T> tail;
    private boolean successAdd;


    /**
     * This is the constructor for the linkedlist
     */
    public LinkedList() {
        head = new LinkedList.Node<T>(null);
        tail = new LinkedList.Node<T>(null);
        head.setNext(tail);
        tail.setPrevious(head);
        size = 0;
        successAdd = false;
    }


    /**
     * This will be the add method for a certain index
     * 
     * @param index
     *            the index the data is being added
     * @param data
     *            the data being added to the list
     * @return true if the add was successful
     */
    public boolean add(int index, T data) {
        if (index < 0 || size < index) {
            throw new IndexOutOfBoundsException();
        }
        if (data == null) {
            throw new IllegalArgumentException("Can't add null item");
        }
        Node<T> nodeAfter;
        if (index == size) {
            nodeAfter = tail;
        }
        else {
            nodeAfter = getNodeAtIndex(index);
        }
        Node<T> insertion = new Node<T>(data);
        insertion.setPrevious(nodeAfter.previous());
        insertion.setNext(nodeAfter);
        nodeAfter.previous().setNext(insertion);
        nodeAfter.setPrevious(insertion);
        size++;
        successAdd = true;
        return successAdd;
    }


    /**
     * This will add an entry to the end of the list
     * 
     * @param data
     *            the data being added
     * @return true if the add was successful
     */
    public boolean add(T data) {
        add(size, data);
        return successAdd;
    }


    /**
     * This checks the list to be empty
     * 
     * @return true if it is empty
     */
    public boolean isEmpty() {
        return (size == 0);
    }


    /**
     * The will return the size of the list
     * 
     * @return the size
     */
    public int size() {
        return size;
    }


    /**
     * This will clear the entire list
     */
    public void clear() {
        head = new LinkedList.Node<T>(null);
        tail = new LinkedList.Node<T>(null);
        head.setNext(tail);
        tail.setPrevious(head);
        size = 0;
        successAdd = false;
    }


    /**
     * This method will return true if the list contains the data
     * 
     * @param data
     *            the data being checked in the list
     * @return true if the data is in the list
     */
    public boolean contains(T data) {
        return (lastIndexOf(data) != -1);
    }


    /**
     * This will get the index of the data wanted
     * 
     * @param index
     *            where the data is located
     * @return the data at the index
     */
    public T get(int index) {
        return (getNodeAtIndex(index).data());
    }


    /**
     * This method will get the node at the index specified
     * 
     * @param index
     *            the index for the node
     * @return the node wanted
     */
    private Node<T> getNodeAtIndex(int index) {
        if (index < 0 || size() <= index) {
            throw new IndexOutOfBoundsException("Index doesn't exist");
        }
        Node<T> currentNode = head.next();
        for (int i = 0; i < index; i++) {
            currentNode = currentNode.next();
        }
        return currentNode;
    }


    /**
     * This method will get the last instance of data
     * 
     * @param data
     *            the data being checked for
     * @return the index of the data
     */
    public int lastIndexOf(T data) {
        Node<T> currentNode = tail.previous();
        for (int i = size() - 1; i >= 0; i--) {
            if (currentNode.data().equals(data)) {
                return i;
            }
            currentNode = currentNode.previous();
        }
        return -1;
    }


    /**
     * This method will remove an item from the list at the index
     * 
     * @param index
     *            the index of the item being removed
     * @return true if the remove was successful
     */
    public boolean remove(int index) {
        Node<T> removed = getNodeAtIndex(index);
        removed.previous().setNext(removed.next());
        removed.next().setPrevious(removed.previous());
        size--;
        return true;
    }


    /**
     * This method will remove the specified item
     * 
     * @param data
     *            the item being removed
     * @return true if the remove was successful
     */
    public boolean remove(T data) {
        Node<T> currentNode = head.next();
        while (!currentNode.equals(tail)) {
            if (currentNode.data().equals(data)) {
                currentNode.previous().setNext(currentNode.next());
                currentNode.next().setPrevious(currentNode.previous());
                size--;
                return true;
            }
            currentNode = currentNode.next();
        }
        return false;
    }


    /**
     * This method will swap the string at index = x
     * with the string at index = y
     * 
     * @param x
     *            the value getting replaced
     * @param y
     *            the value taking the place of x
     */
    public void swap(int x, int y) {
        T firstSong = this.get(x);
        T swappingSong = this.get(y);
        this.remove(firstSong);
        this.remove(swappingSong);
        this.add(x, swappingSong);
        this.add(y, firstSong);
    }


    /**
     * This method will replace the data at index = i with the replacer entry
     * 
     * @param i the index that wants to get replaced
     * @param replacer the entry getting put in place
     */
    public void replace(int i, T replacer) {
        T removedData = this.get(i);
        this.add(i, replacer);
        this.remove(removedData);
    }


    /**
     * This will create the to string for the list
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{");
        if (!isEmpty()) {
            Node<T> currentNode = head.next();
            while (currentNode != tail) {
                T nodeData = currentNode.data();
                builder.append(nodeData.toString());
                if (currentNode.next() != tail) {
                    builder.append(", ");
                }
                currentNode = currentNode.next();
            }
        }
        builder.append("}");
        return builder.toString();
    }


    /**
     * This method will return the iterator
     */
    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator<T>();
    }
}