//Virginia Tech Honor Code Pledge:
//
//As a Hokie, I will conduct myself with 
//honor and integrity at all times.
//I will not lie, cheat, or steal, nor 
//will I accept the actions of those who do.
//-- Pau Lleonart Calvo (paullc)

package prj5;

/**
* The position enumerator helps with being
* able to distinguish between the different
* graph positions. 
* 
* @author Pau Lleonart Calvo (paullc)
* @version 10/21/2019
*
*/
public enum PositionEnum {
    TLEFT, TMIDDLE, TRIGHT, MLEFT, MMIDDLE, MRIGHT, BLEFT, BMIDDLE, BRIGHT, DEFAULT;
}
