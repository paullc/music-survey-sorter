//Virginia Tech Honor Code Pledge:
//
//As a Hokie, I will conduct myself with 
//honor and integrity at all times.
//I will not lie, cheat, or steal, nor 
//will I accept the actions of those who do.
//-- Pau Lleonart Calvo (paullc)

package prj5;

/**
 * The figure class simplifies the displaying behavior of the
 * GUI_DisplayWindow class by segmenting what data needs to be presented
 * at which position for each glyph.
 * 
 * 
 * @author Pau Lleonart Calvo (paullc)
 * 
 * @version 11/27/2019
 *
 */
public class Figure {
    
    /**
     * The title of the song to be displayed by this Figure.
     */
    private String title;
    
    /**
     * The year of the song to be displayed by this Figure.
     */
    private String year;
    
    /**
     * The genre of the song to be displayed by this Figure.
     */
    private String genre;
    
    /**
     * The artist of the song to be displayed by this Figure.
     */
    private String artist;
    
    /**
     * An array of doubles where indexes 0-3 are percentages of Students
     * who heard the song and 4-8 are percentages of students who 
     * liked the song.
     * 
     * The order is 1-4 1-4 in the same order as is displayed in the GUI legend.
     */
    private double[] percent;
    
    /**
     * The glyph position of this Figure.
     */
    private PositionEnum position;
    
    /**
     * The Figure constructor which initializes the necessary fields
     * from its parameters
     * 
     * @param pos The PositionEnum position of this Figure.
     * @param percentages The percentages array of heard and like percentages.
     * @param title The title of the song.
     * @param artist The artist of the song.
     * @param year The year of the song.
     * @param genre The genre of the song.
     */
    public Figure(PositionEnum pos, double[] percentages, String title, 
        String artist, String year, String genre) {
        
        this.position = pos;
        this.percent = percentages;
        this.title = title;
        this.artist = artist;
        this.year = year;
        this.genre = genre;
    }
    
    /**
     * Returns the song genre.
     * 
     * @return A String
     */
    public String getGenre() {
        return this.genre;
    }
    
    /**
     * Returns the release year of the song.
     * 
     * @return A String
     */
    public String getYear() {
        return this.year;
    }
    
    /**
     * Returns the title of the song.
     * 
     * @return A String
     */
    public String getTitle() {
        return this.title;
    }
    
    /**
     * Returns the artist of the song.
     * 
     * @return A String
     */
    public String getArtist() {
        return this.artist;
    }
    
    /**
     * Returns the PositionEnum position
     * of this Figure.
     * 
     * @return A PositionEnum position.
     */
    public PositionEnum getPosition() {
        return this.position;
    }
    
    /**
     * Returns the desired percentage depending
     * on the passed integer parameter. 
     * 
     * The index of the array is equivalent to
     * the int parameter - 1.
     * 
     * @param num The integer parameter used to reference
     * the desired heard percentages.
     * 
     * @return The heard percentage as an integer.
     */
    public int getHeard(int num) {
        switch (num) {
            case 1:
                return (int) this.percent[0];
            case 2:
                return (int) this.percent[1];
            case 3:
                return (int) this.percent[2];
            case 4:
                return (int) this.percent[3];
            default:
                return (int) this.percent[0];
        }
    }
    
    /**
     * Returns the desired percentage depending
     * on the passed integer parameter. 
     * 
     * The index of the array is equivalent to
     * the int parameter - 1.
     * 
     * @param num The integer parameter used to reference
     * the desired liked percentages.
     * 
     * @return The liked percentage as an integer.
     */
    public int getLiked(int num) {
        switch (num) {
            case 1:
                return (int) this.percent[4];
            case 2:
                return (int) this.percent[5];
            case 3:
                return (int) this.percent[6];
            case 4:
                return (int) this.percent[7];
            default:
                return (int) this.percent[4];
        }
    }
    
    
    
    
}
