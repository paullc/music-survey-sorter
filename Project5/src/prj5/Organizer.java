// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those who
// do.
// -- Pau Lleonart Calvo (paullc)
package prj5;

import java.util.Iterator;

/**
 * The organizer class handles the back end data
 * organization for songs and students depending on the user's
 * use of the GUI.
 * 
 * @author Pau Lleonart Calvo (paullc)
 * 
 * @version 11/18/2019
 *
 */
public class Organizer {
    
    /**
     * A Boolean representing whether or not there remain
     * any pages of song data to be displayed in the GUI.
     */
    private boolean morePages;
    
    /**
     * An array of Figure objects to be used by the GUI
     * to make glyphs in the 9 regions of the window.
     */
    private Figure[] figures;
    
    /**
     * Integer representing the current page number the user
     * is viewing in the GUI.
     */
    private int page;

    /**
     * The linked list of student objects to be sorted.
     */
    private LinkedList<Student> students;

    /**
     * The linked list of song objects to be sorted.
     */
    private LinkedList<Song> songs;

    /**
     * A sorted copy of the songs field according
     * to what parameter the user has decided to sort by.
     */
    private LinkedList<Song> sortedSongs;


    /**
     *  The organizer constructor initializes the 
     *  student and song LinkedList fields
     *  and the Figures array. This data is then 
     *  used to sort and display relevant
     *  data to the user.
     * 
     * @param students The Linked List of Student objects.
     * 
     * @param songs The Linked List of Song objects.
     */
    public Organizer(LinkedList<Student> students, LinkedList<Song> songs) {
        this.students = students;
        this.songs = songs;
        this.figures = new Figure[9];
        this.page = 0;
        this.morePages = true;
    }
    
    /**
     * A boolean indicating whether or not
     * there are more song pages to dispaly.
     * 
     * @return A boolean
     */
    public boolean hasMorePages() {
        return this.morePages;
    }
    
    /**
     * Returns the array of figure objects 
     * Order: TLEFT (top left) to BRIGHT (bottom right)
     * 
     * @return The array of figures
     */
    public Figure[] getFigures() {
        return this.figures;
    }


    /**
     * Returns the students linked list field
     * for this Organizer object.
     * 
     * @return A Linked List of student objects
     */
    public LinkedList<Student> getStudents() {
        return this.students;
    }


    /**
     * Returns the songs Linked List field
     * for this Organizer object.
     * 
     * @return A linked list of song objects
     */
    public LinkedList<Song> getSongs() {
        return this.songs;
    }
    
    /**
     * Returns the current page number.
     * 
     * @return The current page number
     */
    public int getPage() {
        return this.page;
    }


    /**
     * Sorts songs according to a string parameter
     * indicating what category to sort by.
     * 
     * 
     * @param sortable The parameter to sort by. Distinct options
     * are "title", "artist", "genre", "year".
     * 
     * @return Returns a Linked List of Song objects
     *         sorted in the order indicated by the String parameter.
     */
    public LinkedList<Song> sortSongsBy(String sortable) {
        LinkedList<Song> sorted = new LinkedList<Song>();
        Iterator<Song> iter = this.songs.iterator();

        // Add songs from original data to new linked list "sorted"
        while (iter.hasNext()) {
            sorted.add(iter.next());
        }
        
        sortable = sortable.toLowerCase().trim();
        
        switch (sortable) {
            case "title": {

                for (int i = 1; i < sorted.size(); i++) {

                    for (int j = i; j > 0; j--) {
                        if (sorted.get(j).getSongTitle().compareTo(sorted.get(j
                            - 1).getSongTitle()) < 0) {
                            sorted.swap(j - 1, j);
                        }
                    }
                }
                break;
            }
            case "artist": {

                for (int i = 1; i < sorted.size(); i++) {

                    for (int j = i; j > 0; j--) {
                        if (sorted.get(j).getArtist().compareTo(sorted.get(j
                            - 1).getArtist()) < 0) {
                            sorted.swap(j - 1, j);
                        }
                    }
                }
                break;
            }
            case "year": {

                for (int i = 1; i < sorted.size(); i++) {

                    for (int j = i; j > 0; j--) {
                        if (sorted.get(j).getDate().compareTo(sorted.get(j - 1)
                            .getDate()) < 0) {
                            sorted.swap(j - 1, j);
                        }
                    }
                }
                break;
            }
            case "genre": {

                for (int i = 1; i < sorted.size(); i++) {

                    for (int j = i; j > 0; j--) {
                        if (sorted.get(j).getGenre().compareTo(sorted.get(j - 1)
                            .getGenre()) < 0) {
                            sorted.swap(j - 1, j);
                        }
                    }
                }
                break;
            }
            default: {
                for (int i = 1; i < sorted.size(); i++) {

                    for (int j = i; j > 0; j--) {
                        if (sorted.get(j).getSongTitle().compareTo(sorted.get(j
                            - 1).getSongTitle()) < 0) {
                            sorted.swap(j - 1, j);
                        }
                    }
                }
                break;
            }

        }
        this.sortedSongs = sorted;
        return sorted;

    }


    /**
     * Organizes displayed data by Student hobbies.
     * 
     */
    public void showByHobby() {
        this.figures = new Figure[9];
        
        for (int i = (this.getPage() * 9); i < 
            ((this.getPage() + 1) * 9); i++) {
            
            Song song;
            
            try {
                song = this.sortedSongs.get(i);
            }
            catch (Exception e) {
                this.morePages = false;
                break;
            }

            Iterator<Student> iter = this.students.iterator();

            //-------------------
            int heardRead = 0;
            int heardArt = 0;
            int heardSports = 0;
            int heardMusic = 0;
            //-------------------
            int likeRead = 0;
            int likeArt = 0;
            int likeSports = 0;
            int likeMusic = 0;
            //-------------------
            int totalRead = 0;
            int totalArt = 0;
            int totalSports = 0;
            int totalMusic = 0;
            //-------------------

            while (iter.hasNext()) {
                Student student = iter.next();
                int heard = student.getHeard(song.getOriginalIndex());
                int like = student.getLikes(song.getOriginalIndex());
                
                if (student.getHobby().equals("reading")) {
                    if (heard == 1) {
                        heardRead++;
                    }
                    if (like == 1) {
                        likeRead++;
                    }
                    if (heard != -1 || like != -1) {
                        totalRead++;
                    }
                }
                else if (student.getHobby().equals("art")) {
                    if (heard == 1) {
                        heardArt++;
                    }
                    if (like == 1) {
                        likeArt++;
                    }
                    if (heard != -1 || like != -1) {
                        totalArt++;
                    }
                }
                else if (student.getHobby().equals("sports")) {
                    if (heard == 1) {
                        heardSports++;
                    }
                    if (like == 1) {
                        likeSports++;
                    }
                    if (heard != -1 || like != -1) {
                        totalSports++;
                    }
                }
                else if (student.getHobby().equals("music")) {
                    if (heard == 1) {
                        heardMusic++;
                    }
                    if (like == 1) {
                        likeMusic++;
                    }

                    if (heard != -1 || like != -1) {
                        totalMusic++;
                    }
                }

            }

            //--- Heard Percentages -----------------------------------------
            double readingH = (double)heardRead / totalRead * 100;
            double musicH = (double)heardMusic / totalMusic * 100;
            double artH = (double)heardArt / totalArt * 100;
            double sportsH = (double)heardSports / totalSports * 100;
            
            //--- Read Percentages ------------------------------------------
            double readingL = (double)likeRead / totalRead * 100;
            double musicL = (double)likeMusic / totalMusic * 100;
            double artL = (double)likeArt / totalArt * 100;
            double sportsL = (double)likeSports / totalSports * 100;
            
            PositionEnum pos;
            
            switch (i % 9) {
                case 0:
                    pos = PositionEnum.TLEFT;
                    break;
                case 1:
                    pos = PositionEnum.TMIDDLE;
                    break;
                case 2:
                    pos = PositionEnum.TRIGHT;
                    break;
                case 3:
                    pos = PositionEnum.MLEFT;
                    break;
                case 4:
                    pos = PositionEnum.MMIDDLE;
                    break;
                case 5:
                    pos = PositionEnum.MRIGHT;
                    break;
                case 6:
                    pos = PositionEnum.BLEFT;
                    break;
                case 7:
                    pos = PositionEnum.BMIDDLE;
                    break;
                case 8:
                    pos = PositionEnum.BRIGHT;
                    break;
                default:
                    pos = PositionEnum.MMIDDLE;
                    break;
                    
            }
            
            double[] percentArray = new double[] {
                readingH, artH, sportsH, musicH,
                readingL, artL, sportsL, musicL};
            
            
            this.figures[i % 9] = new Figure(
                pos, percentArray, song.getSongTitle(), 
                song.getArtist(), song.getDate(), 
                song.getGenre());
            
            

        }
    }
    
    
    /**
     * Organizes displayed data by Student region.
     * 
     */
    public void showByRegion() {
        this.figures = new Figure[9];
        
        for (int i = (this.getPage() * 9); i < 
            ((this.getPage() + 1) * 9); i++) {
            
            Song song;
            
            try {
                song = this.sortedSongs.get(i);
            }
            catch (Exception e) {
                this.morePages = false;
                break;
            }
            
            Iterator<Student> iter = this.students.iterator();

            //-------------------
            int heardSouth = 0;
            int heardNorth = 0;
            int heardRest = 0;
            int heardOutside = 0;
            //-------------------
            int likeSouth = 0;
            int likeNorth = 0;
            int likeRest = 0;
            int likeOutside = 0;
            //-------------------
            int totalSouth = 0;
            int totalNorth = 0;
            int totalRest = 0;
            int totalOutside = 0;
            //-------------------

            while (iter.hasNext()) {
                Student student = iter.next();
                int heard = student.getHeard(song.getOriginalIndex());
                int like = student.getLikes(song.getOriginalIndex());
                
                if (student.getRegion().equals("southeast")) {
                    if (heard == 1) {
                        heardSouth++;
                    }
                    if (like == 1) {
                        likeSouth++;
                    }
                    if (heard != -1 || like != -1) {
                        totalSouth++;
                    }
                }
                else if (student.getRegion().equals("northeast")) {
                    if (heard == 1) {
                        heardNorth++;
                    }
                    if (like == 1) {
                        likeNorth++;
                    }
                    if (heard != -1 || like != -1) {
                        totalNorth++;
                    }
                }
                else if (student.getRegion().equals(
                    "united states " + 
                    "(other than southeast or northwest)")) {
                    
                    if (heard == 1) {
                        heardRest++;
                    }
                    if (like == 1) {
                        likeRest++;
                    }
                    if (heard != -1 || like != -1) {
                        totalRest++;
                    }
                }
                else if (student.getRegion().equals(
                    "outside of united states")) {
                    if (heard == 1) {
                        heardOutside++;
                    }
                    if (like == 1) {
                        likeOutside++;
                    }

                    if (heard != -1 || like != -1) {
                        totalOutside++;
                    }
                }

            }

            //--- Heard Percentages -----------------------------------------
            double southH = (double)heardSouth / totalSouth * 100;
            double outsideH = (double)heardOutside / totalOutside * 100;
            double northH = (double)heardNorth / totalNorth * 100;
            double restH = (double)heardRest / totalRest * 100;
            
            //--- Read Percentages ------------------------------------------
            double southL = (double)likeSouth / totalSouth * 100;
            double outsideL = (double)likeOutside / totalOutside * 100;
            double northL = (double)likeNorth / totalNorth * 100;
            double restL = (double)likeRest / totalRest * 100;
            
            PositionEnum pos;
            
            switch (i % 9) {
                case 0:
                    pos = PositionEnum.TLEFT;
                    break;
                case 1:
                    pos = PositionEnum.TMIDDLE;
                    break;
                case 2:
                    pos = PositionEnum.TRIGHT;
                    break;
                case 3:
                    pos = PositionEnum.MLEFT;
                    break;
                case 4:
                    pos = PositionEnum.MMIDDLE;
                    break;
                case 5:
                    pos = PositionEnum.MRIGHT;
                    break;
                case 6:
                    pos = PositionEnum.BLEFT;
                    break;
                case 7:
                    pos = PositionEnum.BMIDDLE;
                    break;
                case 8:
                    pos = PositionEnum.BRIGHT;
                    break;
                default:
                    pos = PositionEnum.MMIDDLE;
                    break;
                    
            }
            double[] percentArray = new double[] {
                northH, southH, restH, outsideH,
                northL, southL, restL, outsideL};
            
            
            this.figures[i % 9] = new Figure(
                pos, percentArray, song.getSongTitle(), 
                song.getArtist(), song.getDate(), 
                song.getGenre());
            
        }
    }

    
    /**
     * Organizes displayed data by Student major.
     * 
     */
    public void showByMajor() {
        
        this.figures = new Figure[9];
        
        for (int i = (this.getPage() * 9); i < 
            ((this.getPage() + 1) * 9); i++) {
            
            Song song;
            
            try {
                song = this.sortedSongs.get(i);
            }
            catch (Exception e) {
                this.morePages = false;
                break;
            }

            Iterator<Student> iter = this.students.iterator();

            //-------------------
            int heardComputer = 0;
            int heardEngineering = 0;
            int heardMorC = 0;
            int heardOther = 0;
            //-------------------
            int likeComputer = 0;
            int likeEngineering = 0;
            int likeMorC = 0;
            int likeOther = 0;
            //-------------------
            int totalComputer = 0;
            int totalEngineering = 0;
            int totalMorC = 0;
            int totalOther = 0;
            //-------------------

            while (iter.hasNext()) {
                Student student = iter.next();
                int heard = student.getHeard(song.getOriginalIndex());
                int like = student.getLikes(song.getOriginalIndex());
                
                if (student.getMajor().equals("computer science")) {
                    if (heard == 1) {
                        heardComputer++;
                    }
                    if (like == 1) {
                        likeComputer++;
                    }
                    if (heard != -1 || like != -1) {
                        totalComputer++;
                    }
                }
                else if (student.getMajor().equals("other engineering")) {
                    if (heard == 1) {
                        heardEngineering++;
                    }
                    if (like == 1) {
                        likeEngineering++;
                    }
                    if (heard != -1 || like != -1) {
                        totalEngineering++;
                    }
                }
                else if (student.getMajor().equals("math or cmda")) {
                    if (heard == 1) {
                        heardMorC++;
                    }
                    if (like == 1) {
                        likeMorC++;
                    }
                    if (heard != -1 || like != -1) {
                        totalMorC++;
                    }
                }
                else if (student.getMajor().equals("other")) {
                    if (heard == 1) {
                        heardOther++;
                    }
                    if (like == 1) {
                        likeOther++;
                    }

                    if (heard != -1 || like != -1) {
                        totalOther++;
                    }
                }

            }

            //--- Heard Percentages -----------------------------------------
            double computerH = (double)heardComputer / totalComputer * 100;
            double otherH = (double)heardOther / totalOther * 100;
            double engineeringH = (double)heardEngineering / 
                totalEngineering * 100;
            double morcH = (double)heardMorC / totalMorC * 100;
            
            //--- Read Percentages ------------------------------------------
            double computerL = (double)likeComputer / totalComputer * 100;
            double otherL = (double)likeOther / totalOther * 100;
            double engineeringL = (double)likeEngineering / 
                totalEngineering * 100;
            double morcL = (double)likeMorC / totalMorC * 100;
            
            PositionEnum pos;
            
            switch (i % 9) {
                case 0:
                    pos = PositionEnum.TLEFT;
                    break;
                case 1:
                    pos = PositionEnum.TMIDDLE;
                    break;
                case 2:
                    pos = PositionEnum.TRIGHT;
                    break;
                case 3:
                    pos = PositionEnum.MLEFT;
                    break;
                case 4:
                    pos = PositionEnum.MMIDDLE;
                    break;
                case 5:
                    pos = PositionEnum.MRIGHT;
                    break;
                case 6:
                    pos = PositionEnum.BLEFT;
                    break;
                case 7:
                    pos = PositionEnum.BMIDDLE;
                    break;
                case 8:
                    pos = PositionEnum.BRIGHT;
                    break;
                default:
                    pos = PositionEnum.MMIDDLE;
                    break;
                    
            }
            double[] percentArray = new double[] {
                computerH, engineeringH, morcH, otherH,
                computerL, engineeringL, morcL, otherL};
            
            
            this.figures[i % 9] = new Figure(
                pos, percentArray, song.getSongTitle(), 
                song.getArtist(), song.getDate(), 
                song.getGenre());
            
        }
    }
    
    /**
     * Increments the page by 1 unless the page
     * maximum has been reached.
     */
    public void nextPage() {
        if (this.hasMorePages()) {
            this.page++;
        }
    }
    
    /**
     * Decrements the page by 1 unless the page
     * minimum has been reached.
     */
    public void previousPage() {
        if (this.page > 0) {
            this.page--;
            this.morePages = true;
        }
    }
}