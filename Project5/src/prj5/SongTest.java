// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those
// who do.
// -- Nathan Craig (nlcraig)
package prj5;

import student.TestCase;

/**
 * @author Nathan Craig (nlcraig)
 * @version 2019.11.17
 *
 *          Test class for the methods of the Song class. Includes test cases
 *          for all
 *          methods and possible method outcomes of those in the Song class.
 *
 */
public class SongTest extends TestCase {
    private Song testSong;


    /**
     * Set up the conditions to be used in each test case.
     */
    public void setUp() {
        testSong = new Song("22", "Taylor Swift", "2012", "Pop", 0);
    }


    /**
     * Test the getter method that returns the artists of a Song.
     */
    public void testGetArtist() {
        assertEquals("Taylor Swift", testSong.getArtist());
    }


    /**
     * Test the getter method that returns the title of a Song.
     */
    public void testGetSongTitle() {
        assertEquals("22", testSong.getSongTitle());
    }


    /**
     * Test the getter method that returns the genre of a Song.
     */
    public void testGetGenre() {
        assertEquals("Pop", testSong.getGenre());
    }


    /**
     * Test the getter method that returns the release year of a song.
     */
    public void testGetDate() {
        assertEquals("2012", testSong.getDate());
    }


    /**
     * Test the toString method to ensure that a proper Sting render of
     * a Song is returned in the correct format.
     */
    public void testToString() {
        assertEquals("[Title: 22 Artist: Taylor Swift Genre: Pop Date: 2012]",
            testSong.toString());
    }


    /**
     * Test the overridden equals method in the case that a Song is being
     * compared to a null Object.
     */
    public void testEqualsNull() {
        Object obj = null;
        assertFalse(testSong.equals(obj));
    }


    /**
     * Test the overridden equals method in the case that a Song is being
     * compared to an identical Song
     */
    public void testEqualsIdentical() {
        Song compare = testSong;
        assertEquals(testSong, compare);
    }


    /**
     * Test the overridden equals method in the cases that a Song is being
     * compared to a Song with equivalent attributes and Songs that have
     * differing attributes.
     */
    public void testEquals() {
        Song compare1 = new Song("22", "Taylor Swift", "2012", "Pop", 1);
        assertEquals(testSong, compare1);

        Song compare2 = new Song("Drake", "22", "Pop", "2012", 2);
        assertFalse(testSong.equals(compare2));

        Song compare3 = new Song("Taylor Swift", "Love Song", "Pop", "2012", 3);
        assertFalse(testSong.equals(compare3));

        Song compare4 = new Song("Taylor Swift", "22", "Rap", "2012", 4);
        assertFalse(testSong.equals(compare4));

        Song compare5 = new Song("Taylor Swift", "22", "Pop", "2013", 5);
        assertFalse(testSong.equals(compare5));

        Song compare6 = new Song("22", "Taylor Swift", "Rap", "2012", 6);
        assertFalse(testSong.equals(compare6));

        Song compare7 = new Song("22", "Taylor Swift", "Pop", "2", 7);
        assertFalse(testSong.equals(compare7));

        Song compare8 = new Song("22", "Someone", "Pop", "2012", 8);
        assertFalse(testSong.equals(compare8));

        Song compare9 = new Song("11", "Taylor Swift", "Pop", "2012", 9);
        assertFalse(testSong.equals(compare9));

        Song compare10 = new Song("22", "Taylor Swift", "2012", "Pop", 10);
        assertTrue(testSong.equals(compare10));
        
        Song compare11 = new Song("22", "Taylor Swift", "2012", "pop", 0);
        assertFalse(testSong.equals(compare11));
        
        Song compare12 = new Song("22", "Taylor Swift", "2011", "Pop", 0);
        assertFalse(testSong.equals(compare12));
    }


    /**
     * Test the overridden equals method in the case that a Song is being
     * compared to an Object that is not an instance of the Song class.
     */
    public void testEqualsDiffClass() {
        Object obj = new Object();
        assertFalse(testSong.equals(obj));
    }


    /**
     * Test the compare methods to ensure that the proper int value is returned
     * when two Songs have equal and unequal attributes.
     */
    public void testCompare() {
        Song compare1 = new Song("22", "Taylor Swift", "2012", "Pop", 1);
        Song compare2 = new Song("Maroon 5", "This Love", "Alternative", "2002",
            2);

        assertEquals(1, testSong.compareArtist(compare1));
        assertEquals(-1, testSong.compareArtist(compare2));

        assertEquals(1, testSong.compareDate(compare1));
        assertEquals(-1, testSong.compareDate(compare2));

        assertEquals(1, testSong.compareGenre(compare1));
        assertEquals(-1, testSong.compareGenre(compare2));

        assertEquals(1, testSong.compareTitle(compare1));
        assertEquals(-1, testSong.compareTitle(compare2));
    }

    
    /**
     * Test the getOrginalSongIndex methods
     */
    public void testGetOrginalSongIndex() {
        assertEquals(0, testSong.getOriginalIndex());
    }
}