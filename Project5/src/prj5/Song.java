// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those 
// who do.
// -- Nathan Craig (nlcraig)
package prj5;

/**
 * @author Nathan Craig (nlcraig)
 * @version 2019.11.17
 *
 *
 *
 */
public class Song
{
    private String artist;
    private String genre;
    private String title;
    private String date;
    private int originalIndex;
    
    /**
     * Constructor takes parameters to initialize the 
     * 
     * @param artist   The musician/band who performs the Song.
     * @param title   The title of the Song.
     * @param genre   The genre classification of the Song.
     * @param date   The year the Song was released.
     * @param ogIndex The original index position of the song
     */
    public Song(String title, String artist, 
        String date, String genre, int ogIndex)
    {
        this.artist = artist;
        this.title = title;
        this.genre = genre;
        this.date = date;
        this.originalIndex = ogIndex;
    }
    /**
     * Getter method for the original index of the song
     * 
     * @return An integer representing the original ordered
     * index of the song
     */
    public int getOriginalIndex() {
        return this.originalIndex;
    }
    
    
    /**
     * Getter method for returning the artist of a Song.
     * 
     * @return   The artist of a Song.
     */
    public String getArtist()
    {
        return artist;
    }
    
    /**
     * Getter method for returning the title of a Song.
     * 
     * @return   The title of a Song.
     */
    public String getSongTitle()
    {
        return title;
    }
    
    /**
     * Getter method for returning the genre of a Song.
     * 
     * @return   The genre of a Song.
     */
    public String getGenre()
    {
        return genre;
    }
    
    /**
     * Getter method for returning the release year of a Song.
     * 
     * @return   The release year of a Song.
     */
    public String getDate()
    {
        return date;
    }
    
    /**
     * Method creates and returns a String representation of a song using its
     * attributes in the format [Title: _ Artist: _ Genre: _ Date: _] where the
     * underscored represent the subsequent data that will define a Song.
     * 
     * @return   A string representation of a Song.
     */
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("[Title: " + title + " Artist: " + artist + " Genre: "
            + genre + " Date: " + date + "]");
        return builder.toString();
    }
    
    /**
     * Method returns a boolean value depending on whether an object passed as
     * a Parameter is equals to this Song. All attributes of a Song are
     * compared for equality, and cases where the passed parameter is null or
     * not a Song instance are checked as well.
     * 
     * @return   True if the passed Object is equal to this Song, false if not.
     * @param other   The object passed for comparison to this Song.
     */
    public boolean equals(Object other)
    {
        if (other == null)
        {
            return false;
        }
        else if (other == this)
        {
            return true;
        }
        else if (other.getClass() == this.getClass())
        {
            Song cast = (Song) other;
            return (artist.equals(cast.getArtist()) && 
                title.equals(cast.getSongTitle()) && 
                genre.equals(cast.getGenre()) && date.equals(cast.getDate()));
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Method for comparing the artists of two Song objects (this Song and a
     * Song passed as a parameter).
     * 
     * @return   1 if the artists match, -1 if they do not.
     * @param song   The song being compared to.
     */
    public int compareArtist(Song song)
    {
        if (artist.equals(song.getArtist()))
        {
            return 1;
        }
        return -1;
    }
    
    /**
     * Method for comparing the titles of two Song objects (this Song and a
     * Song passed as a parameter).
     * 
     * @return   1 if the titles match, -1 if they do not.
     * @param song   The song being compared to.
     */
    public int compareTitle(Song song)
    {
        if (title.equals(song.getSongTitle()))
        {
            return 1;
        }
        return -1;
    }
    
    /**
     * Method for comparing the release years of two Song objects (this Song 
     * and a Song passed as a parameter).
     * 
     * @return   1 if the release dates match, -1 if they do not.
     * @param song   The song being compared to.
     */
    public int compareDate(Song song)
    {
        if (date.equals(song.getDate()))
        {
            return 1;
        }
        return -1;
    }
    
    /**
     * Method for comparing the genres of two Song objects (this Song and a
     * Song passed as a parameter).
     * 
     * @return   1 if the genres match, -1 if they do not.
     * @param song   The song being compared to.
     */
    public int compareGenre(Song song)
    {
        if (genre.equals(song.getGenre()))
        {
            return 1;
        }
        return -1;
    }
}