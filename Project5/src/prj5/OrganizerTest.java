// Virginia Tech Honor Code Pledge:
//
// As a Hokie, I will conduct myself with honor and integrity at all times.
// I will not lie, cheat, or steal, nor will I accept the actions of those
// who do.
// -- Foad Nachabe (foadn)
package prj5;

import java.util.ArrayList;
import student.TestCase;

/**
 * This will test the organizer class
 * 
 * @author Foad Nachabe <foadn>
 * @version <November 19, 2019>
 *
 */
public class OrganizerTest extends TestCase {
    private Organizer organizer;
    private ArrayList<Integer> heard;
    private ArrayList<Integer> likes;


    /**
     * This will make the setup for the test class
     */
    public void setUp() {
        LinkedList<Song> songs = new LinkedList<Song>();
        LinkedList<Student> students = new LinkedList<Student>();
        songs.add(new Song("22", "Taylor", "2012", "Pop", 0));
        songs.add(new Song("Collard Greens", "ScHoolBoyQ", "2011", "Rap", 1));
        songs.add(new Song("gas", "lil Baby", "2017", "Hip Rap", 2));
        heard = new ArrayList<Integer>();
        likes = new ArrayList<Integer>();
        heard.add(0);
        heard.add(1);
        heard.add(0);
        likes.add(1);
        likes.add(0);
        likes.add(1);
        students.add(new Student(0, "computer science", "southeast", 
            "sports", heard, likes));
        students.add(new Student(1, "math or cmda", "northeast", 
            "reading", heard, likes));
        organizer = new Organizer(students, songs);
    }


    /**
     * This will test the getStudents method
     */
    public void testGetStudents() {
        LinkedList<Student> studentz = new LinkedList<Student>();
        studentz.add(new Student(0, "computer science", "southeast", 
            "sports", heard, likes));
        studentz.add(new Student(1, "math or cmda", "northeast", 
            "reading", heard, likes));
        assertEquals(organizer.getStudents().toString(), studentz.toString());
    }


    /**
     * This will test the getSongs method
     */
    public void testGetSongs() {
        LinkedList<Song> songz = new LinkedList<Song>();
        songz.add(new Song("22", "Taylor", "2012", "Pop", 0));
        songz.add(new Song("Collard Greens", "ScHoolBoyQ", "2011", "Rap", 1));
        songz.add(new Song("gas", "lil Baby", "2017", "Hip Rap", 2));
        
        assertEquals(organizer.getSongs().toString(), songz.toString());
    }


    /**
     * This will test the sort songs method for each type of sort
     */
    public void testSortSongsBy() {
        assertEquals(organizer.sortSongsBy("title").toString(),
            "{[Title: 22 Artist: Taylor Genre: Pop Date: 2012],"
                + " [Title: Collard Greens Artist: ScHoolBoyQ "
                + "Genre: Rap Date: 2011], "
                + "[Title: gas Artist: lil Baby Genre: Hip Rap Date: 2017]}");
        assertEquals(organizer.sortSongsBy("artist").toString(),
            "{[Title: Collard Greens Artist: ScHoolBoyQ "
                + "Genre: Rap Date: 2011], "
                + "[Title: 22 Artist: Taylor Genre: Pop Date: 2012], "
                + "[Title: gas Artist: lil Baby Genre: Hip Rap Date: 2017]}");
        assertEquals(organizer.sortSongsBy("year").toString(),
            "{[Title: Collard Greens Artist: ScHoolBoyQ "
                + "Genre: Rap Date: 2011], "
                + "[Title: 22 Artist: Taylor Genre: Pop Date: 2012], "
                + "[Title: gas Artist: lil Baby Genre: Hip Rap Date: 2017]}");
        assertEquals(organizer.sortSongsBy("genre").toString(),
            "{[Title: gas Artist: lil Baby Genre: Hip Rap Date: 2017], "
                + "[Title: 22 Artist: Taylor Genre: Pop Date: 2012], "
                + "[Title: Collard Greens Artist: ScHoolBoyQ "
                + "Genre: Rap Date: 2011]}");
        assertEquals(organizer.sortSongsBy("").toString(),
            "{[Title: 22 Artist: Taylor Genre: Pop Date: 2012],"
                + " [Title: Collard Greens Artist: ScHoolBoyQ "
                + "Genre: Rap Date: 2011], "
                + "[Title: gas Artist: lil Baby Genre: Hip Rap Date: 2017]}");
        assertEquals(organizer.sortSongsBy("booty").toString(),
            "{[Title: 22 Artist: Taylor Genre: Pop Date: 2012],"
                + " [Title: Collard Greens Artist: ScHoolBoyQ "
                + "Genre: Rap Date: 2011], "
                + "[Title: gas Artist: lil Baby Genre: Hip Rap Date: 2017]}");
    }


    /**
     * This will test a different case in sort method
     */
    public void testSortAgain() {
        organizer.getSongs().clear();
        organizer.getSongs().add(new Song("Collard Greens", "ScHoolBoyQ",
            "2011", "Rap", 1));
        organizer.getSongs().add(new Song("gas", "lil Baby", "2017", "Hip Rap",
            2));
        organizer.getSongs().add(new Song("22", "Taylor", "2012", "Pop", 0));
        assertEquals(organizer.sortSongsBy("title").toString(),
            "{[Title: 22 Artist: Taylor Genre: Pop Date: 2012],"
                + " [Title: Collard Greens Artist: ScHoolBoyQ "
                + "Genre: Rap Date: 2011], "
                + "[Title: gas Artist: lil Baby Genre: Hip Rap Date: 2017]}");
    }
    
    /**
     * Tests the hasMorePages() method
     */
    public void testHasMorePages() {
        assertTrue(organizer.hasMorePages());
    }
    
    /**
     * Tests the getFigures() method.
     */
    public void testGetFigures() {
        organizer.sortSongsBy("title");
        organizer.showByHobby();
        assertEquals(organizer.getFigures().length, 9);
        assertEquals(organizer.getFigures()[0].getTitle(), "22");
        assertEquals(organizer.getFigures()[1].getTitle(), "Collard Greens");
        assertEquals(organizer.getFigures()[2].getTitle(), "gas");
        assertNull(organizer.getFigures()[3]);
    }
    
    /**
     * Tests the getPage() method
     */
    public void testGetPage() {
        assertEquals(0, organizer.getPage());
    }
    
    /**
     * Tests the nextPage() method
     */
    public void testNextPage() {
        assertEquals(0, organizer.getPage());
    }
    
    /**
     * Tests the previousPage() method
     */
    public void testPreviousPage() {
        assertEquals(0, organizer.getPage());
    }
    
    /**
     * Tests the showByRegion() method
     */
    public void testShowByRegion() {
        organizer.sortSongsBy("title");
        organizer.showByRegion();
        
        assertEquals(organizer.getFigures()[0].getHeard(1), 0);
        assertEquals(organizer.getFigures()[0].getLiked(1), 100);
    }
    
    /**
     * Tests the showByHobby() method
     */
    public void testShowByHobby() {
        organizer.sortSongsBy("title");
        organizer.showByHobby();
        
        assertEquals(organizer.getFigures()[0].getHeard(1), 0);
        assertEquals(organizer.getFigures()[0].getLiked(1), 100);
    }
    
    /**
     * Tests the showByMajor() method
     */
    public void testShowByMajor() {
        organizer.sortSongsBy("title");
        organizer.showByMajor();
        
        assertEquals(organizer.getFigures()[0].getHeard(1), 0);
        assertEquals(organizer.getFigures()[0].getLiked(1), 100);
    }
    
    
}
